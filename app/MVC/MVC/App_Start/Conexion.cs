﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace MVC.App_Start
{
    
    public class Conexion
    {
        private SqlConnection con;

        private void connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["MyConnectionDB"].ToString();
            con = new SqlConnection(constr);

        }

        public DataTable na_ExecuteDataTable(string NombreProcedimiento, List<SqlParameter> Parametros = null)
        {
            connection();
            con.Open();
            SqlCommand cmd = new SqlCommand(NombreProcedimiento, con);
            cmd.CommandType = CommandType.StoredProcedure;
            if ((Parametros != null))
            {
                foreach (SqlParameter param in Parametros)
                {
                    cmd.Parameters.Add(param);
                }
            }
         
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public int na_ExecuteNonQuery(string NombreProcedimiento, List<SqlParameter> Parametros= null)
        {
            connection();
            con.Open();
            SqlCommand cmd = new SqlCommand(NombreProcedimiento, con);
            cmd.CommandType = CommandType.StoredProcedure;
            if ((Parametros != null))
            {
                foreach (SqlParameter param in Parametros)
                {
                    cmd.Parameters.Add(param);
                }
            }
           
            int filasAfectadas = 0;
            filasAfectadas = cmd.ExecuteNonQuery();
            con.Close();
            return filasAfectadas;
        }
        public object na_ExecuteScalar(string NombreProcedimiento, List<SqlParameter> Parametros = null)
        {
            connection();
            con.Open();
            SqlCommand cmd = new SqlCommand(NombreProcedimiento, con);
            cmd.CommandType = CommandType.StoredProcedure;
            if ((Parametros != null))
            {
                foreach (SqlParameter param in Parametros)
                {
                    cmd.Parameters.Add(param);
                }
            }
            object Resultado = cmd.ExecuteScalar();
            con.Close();
            return Resultado;
        }




    }
}