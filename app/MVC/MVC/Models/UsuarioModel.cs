﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class UsuarioModel
    {
        public string Empresa { get; set; }
        public string NombreUsuario { get; set; }
        public string Password { get; set; }
        public string Ruc { get; set; }
        public string Dni { get; set; }
        public string Cod_Usu { get; set; }
        public string Ap_Mat { get; set; }
        public string Ap_Pat { get; set; }
        public string Direccion { get; set; }
        public string Fecha_Nac { get; set; }
        public string Tipo { get; set; }// A = administrador , U = Usuario
        public string Codigo_Empresa { get; set; }
        public string Estado { get; set; }

    }
}