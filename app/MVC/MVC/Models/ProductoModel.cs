﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class ProductoModel
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string Unidad { get; set; }
        public string Marca { get; set; }
        public string Precio { get; set; }
        public string Precio_Com { get; set; }
        public string Stock { get; set; }
        public string Grupo { get; set; }
        public string Rubro_Com { get; set; }


     }
}