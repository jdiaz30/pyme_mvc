﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class DetalleModel
    {
        public string Id { get; set; }
        public string Descripcion { get; set; }
        public string Tipo_Tran { get; set; }
        public string Cod_Prod { get; set; }
        public string Precio { get; set; }
        public string Cantidad { get; set; }
        public string Unidad { get; set; }
        public string Marca { get; set; }
        public string Total { get; set; }
             
    }
}