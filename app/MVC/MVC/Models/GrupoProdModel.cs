﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class GrupoProdModel
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string Rubro_Com { get; set; }

    }
}