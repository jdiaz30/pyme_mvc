﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class EmpresaModel
    {
        public string Cod_Rub_Com { get; set; }
        public string Ruc { get; set; }
        public string Razon_Social { get; set; }
        public string Cod_Empresa { get; set; }
        public string Direccion { get; set; }
        public string Rep_Legal { get; set; }
        public string Ubi_Geo { get; set; }

    }
}