﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class UbicacionGeograficaModel
    {
        public string Id { get; set; }
        public string Descripcion { get; set; }  
    }
}