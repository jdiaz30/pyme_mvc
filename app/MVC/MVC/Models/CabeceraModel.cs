﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class CabeceraModel
    {
        public string Codigo { get; set; }
        public string Tipo_Tran { get; set; }
        public string Rubro_Com { get; set; }
        public string Cod_Emp { get; set; }
        public string Cod_Usu { get; set; }
        public string Fecha { get; set; }
        public string Documento { get; set; }
        public string Serie { get; set; }
        public string Numero { get; set; }
        public string Dni_Ruc { get; set; }
        public string Cliente { get; set; }
        public string Cant_Prod { get; set; }
        public string Total { get; set; }

    }
}