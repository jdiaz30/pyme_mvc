﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class AccesoSistemaModel
    {
        public string Id { get; set; }
        public string Fecha { get; set; }
        public string Codigo_Usuario { get; set; }
        public string Tipo { get; set; } // I = ingreso , S = salida (cierre sesion)
        public string Estado { get; set; } // 1 = sesion iniciada , 0 = salida (cierre sesion)
    }
}