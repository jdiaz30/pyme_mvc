﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Session["id_usuario"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return View("Index","_Layout");
        }
	}
}