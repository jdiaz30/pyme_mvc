﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;

using MVC.App_Start;
using MVC.Models;
using MVC.Repository;
using System.Web.Script.Serialization;

namespace MVC.Controllers
{
    public class LoginController : Controller
    {
        UbiGeoRep ubiGeoRep = null;
        RubroComRep rubroComRep = null;
        EmpresaRep empresaRep = null;
        UsuarioRep usuarioRep = null;
        AccesoSistemaRep accesoSistRep = null;
        public ActionResult Index()
        {
            if (Session["id_usuario"] != null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View("Index","_LoginLayout");
        }

        public JsonResult Logeo(string Usuario)
        {
            List<UsuarioModel> usuarioList = new List<UsuarioModel>();
            AccesoSistemaModel mAcceso = new AccesoSistemaModel();
            try
            {
                usuarioRep = new UsuarioRep();
                accesoSistRep = new AccesoSistemaRep();

                JavaScriptSerializer jsUsuario = new JavaScriptSerializer();

                var dataUsuario = jsUsuario.Deserialize<UsuarioModel>(Usuario);

                UsuarioModel mUsuario = new UsuarioModel();
                mUsuario = dataUsuario;

                usuarioList = usuarioRep.Login(mUsuario,true);

                if(usuarioList.Count() > 0){

                    mAcceso.Fecha = DateTime.Today.ToString();
                    mAcceso.Tipo = "I";
                    mAcceso.Codigo_Usuario = Session["id_usuario"].ToString();
                    mAcceso.Estado = "1";

                    accesoSistRep.Registrar(mAcceso);

                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }
  
            return Json(usuarioList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ValidaLogeo(string dni)
        {
            List<AccesoSistemaModel> accesoList = new List<AccesoSistemaModel>();
            AccesoSistemaModel mAcceso = new AccesoSistemaModel();
            try
            {
                accesoSistRep = new AccesoSistemaRep();

                mAcceso.Codigo_Usuario = dni;
                mAcceso.Estado = "1";
                mAcceso.Tipo = "I";
                mAcceso.Fecha = DateTime.Today.ToString("d");

                accesoList = accesoSistRep.validaAcceso(mAcceso);

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(accesoList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CodigoEmpresa()
        {
            var codigo = "";
            try
            {
                empresaRep = new EmpresaRep();

                codigo = empresaRep.CodigoEmpresa();

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(codigo, JsonRequestBehavior.AllowGet);

        }

        public JsonResult ListarRubroComercial()
        {
            List<RubroComercialModel> rubroCom = new List<RubroComercialModel>();

            try
            {
                rubroComRep = new RubroComRep();
                rubroCom = rubroComRep.Listar();

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }
            

            return Json(rubroCom, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListarDepartamento()
        {
            List<UbiGeoModel> ubicacion = new List<UbiGeoModel>();

            try
            {
                ubiGeoRep = new UbiGeoRep();

                ubicacion = ubiGeoRep.ListarDepartamento();

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }
            
            return Json(ubicacion, JsonRequestBehavior.AllowGet);

        }

        public JsonResult ListarProvincia(string departamento)
        {
            List<UbiGeoModel> ubicacion = new List<UbiGeoModel>();

            try
            {
                ubiGeoRep = new UbiGeoRep();

                ubicacion = ubiGeoRep.ListarProvincia(departamento);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(ubicacion, JsonRequestBehavior.AllowGet);

        }

        public JsonResult ListarDistrito(string departamento,string provincia)
        {
            List<UbiGeoModel> ubicacion = new List<UbiGeoModel>();

            try
            {
                ubiGeoRep = new UbiGeoRep();

                ubicacion = ubiGeoRep.ListarDistrito(departamento,provincia);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(ubicacion, JsonRequestBehavior.AllowGet);

        }

        public JsonResult Logout()
        {
            bool isTrue = false;
            AccesoSistemaModel mAcceso = new AccesoSistemaModel();

            try
            {
               
                isTrue = false;

                accesoSistRep = new AccesoSistemaRep();

                mAcceso.Tipo = "S";
                mAcceso.Estado = "0";
                mAcceso.Codigo_Usuario = Session["id_usuario"].ToString();
                mAcceso.Fecha = DateTime.Today.ToString();

                accesoSistRep.Registrar(mAcceso);

                mAcceso.Tipo = "I";
                mAcceso.Codigo_Usuario = Session["id_usuario"].ToString();
                mAcceso.Fecha = DateTime.Today.ToString("d");

                accesoSistRep.Actualizar(mAcceso);

                Session.Remove("nombre_usuario");
                Session.Remove("id_usuario");
                Session.Remove("empresa");
                Session.Remove("cod_empresa");
                Session.Remove("rubro_com");
                Session.Remove("tipo_user");
                Session.Remove("licencia");
                Session.Remove("dir_empresa");
                Session.Remove("ruc");

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(isTrue, JsonRequestBehavior.AllowGet);

        }

        public JsonResult RegistrarEmpresa(string Empresa)
        {
            int result = 0;
            try
            {
                empresaRep = new EmpresaRep();

                JavaScriptSerializer jsEmpresa = new JavaScriptSerializer();

                var dataEmpresa = jsEmpresa.Deserialize<EmpresaModel>(Empresa);

                EmpresaModel mEmpresa = new EmpresaModel();
                mEmpresa = dataEmpresa;

                result = empresaRep.Registrar(mEmpresa);

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public JsonResult CodigoUbiGeo(string departamento, string provincia, string distrito)
        {
            var codigo = "";
            try
            {
                ubiGeoRep = new UbiGeoRep();
                codigo = ubiGeoRep.GetCodigoUbicacion(departamento, provincia, distrito);

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(codigo, JsonRequestBehavior.AllowGet);

        }

        public JsonResult RegistrarUsuario(String Usuario)
        {
            var codUsuario = "";
            try
            {
                usuarioRep = new UsuarioRep();

                JavaScriptSerializer jsUsuario = new JavaScriptSerializer();

                var dataUsuario = jsUsuario.Deserialize<UsuarioModel>(Usuario);

                UsuarioModel mUsuario = new UsuarioModel();
                mUsuario = dataUsuario;
                mUsuario.Tipo = "A";

                codUsuario = usuarioRep.Registrar(mUsuario);

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(codUsuario, JsonRequestBehavior.AllowGet);

        }
	}
}