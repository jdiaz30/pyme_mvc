﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MVC.App_Start;
using MVC.Models;
using MVC.Repository;
using System.Web.Script.Serialization;

namespace MVC.Controllers
{
    public class InventarioController : Controller
    {
        GrupoProdRep grupoProd = null;
        UnidadRep unidadRep = null;
        ProductoRep productoRep = null;
        UsuarioRep usuarioRep = null;
        public ActionResult Index()
        {
            if (Session["id_usuario"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            grupoProd = new GrupoProdRep();
            unidadRep = new UnidadRep();

            ViewBag.grupoProd = grupoProd.Listar();
            ViewBag.unidad = unidadRep.Listar();

            return View("Index","_Layout");
        }

        public JsonResult SubGrupoComercial(string rubro)
        {
            List<GrupoProdModel> grupoList = new List<GrupoProdModel>();
            
            try
            {
                grupoProd = new GrupoProdRep();
                grupoList = grupoProd.ListarSubGrupo(rubro);

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(grupoList, JsonRequestBehavior.AllowGet);

        }

        public JsonResult RegistrarProducto(string Producto)
        {
            var respuesta = "";
            try
            {
                productoRep = new ProductoRep();

                JavaScriptSerializer jsProducto = new JavaScriptSerializer();

                var dataProd = jsProducto.Deserialize<ProductoModel>(Producto);

                ProductoModel mProducto = new ProductoModel();
                mProducto = dataProd;

                respuesta = productoRep.Registrar(mProducto);

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(respuesta, JsonRequestBehavior.AllowGet);

        }

        public JsonResult EditarPrecio(double precioCom, double precioVen, string codProd)
        {
            var response = "";
            try
            {
                productoRep = new ProductoRep();

                response = productoRep.EditarPrecio(precioCom,precioVen,codProd);

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(response, JsonRequestBehavior.AllowGet);

        }
        public JsonResult Logeo(string Usuario)
        {
            List<UsuarioModel> usuarioList = new List<UsuarioModel>();
            AccesoSistemaModel mAcceso = new AccesoSistemaModel();
            try
            {
                usuarioRep = new UsuarioRep();

                JavaScriptSerializer jsUsuario = new JavaScriptSerializer();

                var dataUsuario = jsUsuario.Deserialize<UsuarioModel>(Usuario);

                UsuarioModel mUsuario = new UsuarioModel();
                mUsuario = dataUsuario;
                mUsuario.Ruc = Session["ruc"].ToString();

                usuarioList = usuarioRep.Login(mUsuario, false);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(usuarioList, JsonRequestBehavior.AllowGet);
        }

	}
}