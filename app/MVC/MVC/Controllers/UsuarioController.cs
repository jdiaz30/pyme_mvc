﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using MVC.App_Start;
using MVC.Models;
using MVC.Repository;
using System.Web.Script.Serialization;

namespace MVC.Controllers
{
    public class UsuarioController : Controller
    {
        UsuarioRep usuarioRep = null;
        public ActionResult Index()
        {
            if (Session["id_usuario"] == null )
            {
                return RedirectToAction("Index", "Login");
            }
            var tipoUser = Session["tipo_user"].ToString() ;

            if (tipoUser != "A")
            {
                return RedirectToAction("Index", "Home");
            }

            usuarioRep = new UsuarioRep();
            var usuariosList = usuarioRep.Listar(Session["cod_empresa"].ToString());
            var totalUsuarios = usuariosList.Count();

            ViewBag.usuariosList = usuariosList;
            ViewBag.totalUsuarios = totalUsuarios > 1  ? totalUsuarios - 1 : totalUsuarios;

            return View("Index", "_Layout");
        }
        public JsonResult Registrar(String Usuario)
        {
            var codUsuario = "0";
            try
            {
                usuarioRep = new UsuarioRep();

                JavaScriptSerializer jsUsuario = new JavaScriptSerializer();

                var dataUsuario = jsUsuario.Deserialize<UsuarioModel>(Usuario);

                UsuarioModel mUsuario = new UsuarioModel();
                mUsuario = dataUsuario;
                mUsuario.Tipo = "U";
                mUsuario.Codigo_Empresa = Session["cod_empresa"].ToString();

                codUsuario = usuarioRep.Registrar(mUsuario);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(codUsuario, JsonRequestBehavior.AllowGet);

        }
        public JsonResult Editar(String Usuario)
        {
            var codUsuario = "0";
            try
            {
                usuarioRep = new UsuarioRep();

                JavaScriptSerializer jsUsuario = new JavaScriptSerializer();

                var dataUsuario = jsUsuario.Deserialize<UsuarioModel>(Usuario);

                UsuarioModel mUsuario = new UsuarioModel();
                mUsuario = dataUsuario;

                codUsuario = usuarioRep.Editar(mUsuario);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(codUsuario, JsonRequestBehavior.AllowGet);

        }
        public JsonResult EditarEstado(String Usuario)
        {
            var codUsuario = "0";
            try
            {
                usuarioRep = new UsuarioRep();

                JavaScriptSerializer jsUsuario = new JavaScriptSerializer();

                var dataUsuario = jsUsuario.Deserialize<UsuarioModel>(Usuario);

                UsuarioModel mUsuario = new UsuarioModel();
                mUsuario = dataUsuario;

                codUsuario = usuarioRep.EditarEstado(mUsuario);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(codUsuario, JsonRequestBehavior.AllowGet);

        }
	}
}