﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MVC.App_Start;
using MVC.Models;
using MVC.Repository;
using System.Web.Script.Serialization;

namespace MVC.Controllers
{
    public class CompraController : Controller
    {
        TipoDocRep tipoDocRep = null;
        CabeceraRep cabeceraRep = null;
        DetalleRep detalleRep = null;
        public ActionResult Index()
        {
            if (Session["id_usuario"] == null || Request.Browser["IsMobileDevice"] == "true")
            {
                return RedirectToAction("Index", "Login");
            }

            tipoDocRep = new TipoDocRep();

            ViewBag.tipoDoc = tipoDocRep.Listar();

            return View("Index", "_Layout");
        }
        public JsonResult RegistrarCabecera(string Cabecera)
        {
            var respuesta = "01";
            try
            {
                cabeceraRep = new CabeceraRep();
                
                JavaScriptSerializer jsCabecera = new JavaScriptSerializer();

                var dataCabecera = jsCabecera.Deserialize<CabeceraModel>(Cabecera);

                CabeceraModel mCabecera = new CabeceraModel();
                mCabecera = dataCabecera;

                respuesta = cabeceraRep.Registrar(mCabecera);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(respuesta, JsonRequestBehavior.AllowGet);

        }
        public JsonResult RegistrarDetalle(string Detalle)
        {
            var respuesta = "";

            try
            {
                detalleRep = new DetalleRep();

                JavaScriptSerializer jsDetalle = new JavaScriptSerializer();

                var dataDetalle = jsDetalle.Deserialize<DetalleModel>(Detalle);

                DetalleModel mDetalle = new DetalleModel();
                mDetalle = dataDetalle;

                respuesta = detalleRep.Registrar(mDetalle);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(respuesta, JsonRequestBehavior.AllowGet);

        }
	}
}