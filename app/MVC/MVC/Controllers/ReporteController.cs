﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MVC.App_Start;
using MVC.Models;
using MVC.Repository;
using System.Web.Script.Serialization;

namespace MVC.Controllers
{
    public class ReporteController : Controller
    {
        CabeceraRep cabeceraRep = null;
        DetalleRep detalleRep = null;
        public ActionResult Index()
        {
            if (Session["id_usuario"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View("Index", "_Layout");
        }
        public ActionResult Visualizar()
        {
            if (Session["id_usuario"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var opcion = Request.QueryString["opcion"];
            var fechaFin = Request.QueryString["fecha_fin"];
            var fechaIni = Request.QueryString["fecha_ini"];
            var idCabecera = Request.QueryString["id_cabecera"];
            var ruc = Request.QueryString["ruc"];
            var tipo = Request.QueryString["tipo"];

            ViewBag.Opcion = opcion;
            ViewBag.FechaFin = fechaFin;
            ViewBag.FechaIni = fechaIni;
            ViewBag.IdCabecera = idCabecera;
            ViewBag.Ruc = ruc;
            ViewBag.Tipo = tipo;

            if(idCabecera != "" & opcion == "2"){

                System.Diagnostics.Debug.WriteLine("entramos aqui" + idCabecera );

                List<CabeceraModel> cabeceraList = new List<CabeceraModel>();

                cabeceraRep = new CabeceraRep();
                cabeceraList = cabeceraRep.Reporte(idCabecera);

                foreach(var data in cabeceraList){
                    System.Diagnostics.Debug.WriteLine("entramos aqui de nu" + data.Cliente);

                    ViewBag.Cliente = data.Cliente;
                    ViewBag.Fecha = data.Fecha;
                    ViewBag.Ruc = data.Dni_Ruc;
                    ViewBag.Documento = data.Documento;
                }

            }

            return View("Visualizar", "_ReporteLayout");
        }

        public JsonResult BuscarDocumento(string ruc, string fechaIni, string fechaFin, string tipoTran)
        {
            List<CabeceraModel> cabeceraList = new List<CabeceraModel>();

            try
            {
                cabeceraRep = new CabeceraRep();
                cabeceraList = cabeceraRep.Buscar(ruc,fechaIni,fechaFin,tipoTran);

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }

            return Json(cabeceraList, JsonRequestBehavior.AllowGet);

        }
        public JsonResult MostrarDetalleDocumento(string idCorrelativo)
        {
            List<DetalleModel> detalleList = new List<DetalleModel>();

            try
            {
                detalleRep = new DetalleRep();
                detalleList = detalleRep.Buscar(idCorrelativo);

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Controller error" + ex);
            }
            return Json(detalleList, JsonRequestBehavior.AllowGet);
        }

	}
}