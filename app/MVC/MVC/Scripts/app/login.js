﻿$(function () {

    var vLogin = {
        init: function () {

            $("#fec_nac").mask("99/99/9999");
            $("#dni").mask("99999999");

            $("#btn-ingresar").click(function () {
                vLogin.validaLogin();
            });

            $(".txtLogin").keydown(function (event) {
                vLogin.enterTab(event, this);
            });


            $("#TxtDNI").keydown(function (event) {
                solo_numeros(event);
            });

            $("#btn-registrar").click(function (event) {
                vLogin.validaRegistrar();
            });

            $("#TxtRUC").keydown(function (event) {
                solo_numeros(event);
            });

            $("#departamento").change(function (event) {
                var vParam = $("#departamento option:selected").val();
                vLogin.listarProvincia(vParam);

                setTimeout(function () {

                    var vDepa = $("#departamento option:selected").val();
                    var vProv = $("#provincia option:selected").val();

                    vLogin.listarDistrito(vDepa, vProv);
                },1000);

           
            });

            $("#provincia").change(function (event) {
                var vDepa = $("#departamento option:selected").val();
                var vProv = $("#provincia option:selected").val();

                vLogin.listarDistrito(vDepa,vProv);
            });

            $("#TxtClave").keypress(function (event) {
                if (event.keyCode == 13) {
                    vLogin.validaLogin();
                }
            });

            vLogin.listarRubroComercial();
            vLogin.listarDepartamento();

            vLogin.codigoEmpresa();
         
        },
        enterTab: function (event, vThis) {

            if (event.keyCode == 13) {
                $(vThis).parent().next().find(".txtLogin").focus();
            }

        },
        codigoEmpresa: function () {

            $.ajax({
                url: "/login/CodigoEmpresa",
                type: "POST",
                data: {},
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    console.log("codigo "+ data);

                    $("#codigo_empresa").val(data);

                }
            });

        },
        validaLogin : function(){
            var vRuc = $("#TxtRUC").val();
            var vDni = $("#TxtDNI").val();
            var vClave = $("#TxtClave").val();

            if(vRuc == "" || vDni == "" || vClave == "" ){
                sweetAlert("Datos incompletos", "Asegurese de ingresar todos los datos requeridos", "info");
            } else {
               
                vLogin.accesoValidacion(vDni);
            }
        },
        login: function () {

            var vUsuario = {
                Ruc: $("#TxtRUC").val(),
                Dni: $("#TxtDNI").val(),
                Password: $("#TxtClave").val()
            };

            var vData = {
                Usuario: JSON.stringify(vUsuario)
            };

            $.ajax({
                url: "/login/Logeo",
                type: "POST",
                data:JSON.stringify(vData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var usuario = data;
                    if (usuario.length > 0) {

                        $(usuario).each(function () {
                            $("#TxtDNI").val(this.NombreUsuario);
                            $("#TxtRUC").val(this.Empresa);
                        });

                        swal({
                            title: "Mensaje del sistema",
                            text: "Bienvenido!",
                            type: "success",
                            showCancelButton: false,
                            showConfirmButton: false,
                            timer: 2000,
                        });

                        setTimeout(function () {
                            location.href = "/Home"
                        }, 2001);
                
                    } else {
                        sweetAlert("No se pudo ingresar", "Sus credenciales no son correctas", "error");
                    }
                   
                }
            });
        },
        listarRubroComercial: function () {

            $.ajax({
                url: "/login/ListarRubroComercial",
                type: "POST",
                data: {},
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#rubro').html("");
                    $(data).each(function (item, data) {
                        
                        $('#rubro')
                        .append($("<option></option>")
                        .attr("value", this.Id)
                        .text(this.Descripcion));
                    });

                }
            });

        },
        listarDepartamento: function () {

            $.ajax({
                url: "/login/ListarDepartamento",
                type: "POST",
                data: {},
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#departamento').html("");
                    $(data).each(function (item, data) {

                        $('#departamento')
                        .append($("<option></option>")
                        .attr("value", this.Id)
                        .text(this.Descripcion));
                    });

                    var vDepa = $("#departamento option:selected").val();
                    vLogin.listarProvincia(vDepa);

                    setTimeout(function () {

                        var vProv = $("#provincia option:selected").val();
                        vLogin.listarDistrito(vDepa, vProv);

                    }, 1000);

                }
            });

        },
        listarProvincia: function (vDepartamento) {

            var vDataAction = {departamento : vDepartamento};

            $.ajax({
                url: "/login/ListarProvincia",
                type: "POST",
                data: JSON.stringify(vDataAction),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#provincia').html("");
                    $(data).each(function (item, data) {

                        $('#provincia')
                        .append($("<option></option>")
                        .attr("value", this.Id)
                        .text(this.Descripcion));
                    });

                }
            });

        },
        listarDistrito: function (vDepartamento, vProvincia) {

            var vDataAction = { departamento: vDepartamento,provincia : vProvincia };

            $.ajax({
                url: "/login/ListarDistrito",
                type: "POST",
                data: JSON.stringify(vDataAction),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#distrito').html("");
                    $(data).each(function (item, data) {

                        $('#distrito')
                        .append($("<option></option>")
                        .attr("value", this.Id)
                        .text(this.Descripcion));
                    });

                }
            });

        },
        registrarUsuario: function (vUsuario) {

            var vData = {
                Usuario: JSON.stringify(vUsuario)
            };

            $.ajax({
                url: "/Login/RegistrarUsuario",
                type: "POST",
                data: JSON.stringify(vData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    console.log("cod Usuario" + data);

                    if (data == "0") {

                        sweetAlert("No se pudo registrar", "Revise los datos", "error");

                    } else {

                        sweetAlert("Operación exitosa", "Se registro su empresa", "success");

                        $("#mRegistro").modal("hide");

                        $("#TxtRUC").val($("#ruc").val());
                        $("#TxtDNI").val($("#dni").val());
                        $("#TxtClave").val($("#clave").val());

                        setTimeout(function () {

                            vLogin.login();

                        }, 1000);
                    }
                }
            });

        },
        registrarEmpresa: function (vEmpresa,vUsuario) {

            var vData = {
                Empresa : JSON.stringify(vEmpresa)
            };

            $.ajax({
                url: "/Login/RegistrarEmpresa",
                type: "POST",
                data: JSON.stringify(vData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    vLogin.registrarUsuario(vUsuario);

                }
            });

        },
        validaRegistrar: function () {

            var vDni = $("#dni").val();
            var vApPat = $("#ap_pat").val();
            var vApMat = $("#ap_mat").val();
            var vNombres = $("#nombres").val();
            var vClave = $("#clave").val();
            var vDireccion = $("#dir_usu").val();
            var vFecNac = $("#fec_nac").val();

            var vRuc = $("#ruc").val();
            var vRazonSocial = $("#razon_social").val();
            var vRubro = $("#rubro option:selected").val();
            var vDepa = $("#departamento option:selected").val();
            var vProv = $("#provincia option:selected").val();
            var vDist = $("#distrito option:selected").val();
            var vDirEmp = $("#direccion").val();
            var vRepLegal = $("#rep_legal").val();
            var vIdEmpresa = $("#codigo_empresa").val();


            if(vFecNac === ""){
                vFecNac = "1/1/1890";
            }

            var vUsuario = {
                Dni: vDni,
                Ap_Pat: vApPat,
                Ap_Mat: vApMat,
                NombreUsuario: vNombres,
                Password: vClave,
                Direccion: vDireccion,
                Fecha_Nac: vFecNac,
                Codigo_Empresa : vIdEmpresa
            };

            if (vDni == "" || vApMat == "" || vApPat == "" || vNombres == "" || vClave == ""
                || vRazonSocial == "" || vRuc == "") {
                sweetAlert("No se pudo registrar", "Faltan datos", "error");
            } else {

                var vParams = {departamento : vDepa,provincia : vProv, distrito : vDist};

                $.ajax({
                    url: "/Login/CodigoUbiGeo",
                    type: "POST",
                    data: JSON.stringify(vParams),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        console.log("codigo ubigeo : " + data);

                        var idGeo = data;

                        var vEmpresa = {
                            Ubi_Geo: idGeo,
                            Cod_Rub_Com: vRubro,
                            Ruc: vRuc,
                            Razon_Social: vRazonSocial,
                            Rep_Legal: vRepLegal,
                            Direccion: vDirEmp,
                            Cod_Empresa: vIdEmpresa

                        };

                        vLogin.registrarEmpresa(vEmpresa, vUsuario);

                    }
                });

               
            }

        },
        accesoValidacion: function (vDni) {

            var vData = {
                dni: vDni
            };

            $.ajax({
                url: "/login/ValidaLogeo",
                type: "POST",
                data: JSON.stringify(vData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.length > 0) {

                        swal({
                            title: "Mensaje del sistema",
                            text: "Usted ya ha iniciado sesión!",
                            type: "info",
                            showCancelButton: false,
                            showConfirmButton: false,
                            timer: 2000,
                        });

                    } else {
                        vLogin.login();
                    }
                    
                }
            });

        }



    };

    vLogin.init();
});