﻿$(function () {

    var vUsuario = {
        init: function () {
            select_item_menu(5);

            $('[data-toggle="tooltip"]').tooltip();

            $("#fec_nac").mask("99/99/9999");
            $("#dni").mask("99999999");

            $("#btn-registrar").click(function () {
                vUsuario.validaRegistrar();
            });

            $("#btn-usuario-no").click(function () {
                vUsuario.alertaRegistro();
            });

            $("#btn-usuario").click(function () {
                $("#title-modal-reg").html("Nuevo usuario");
                vUsuario.limpiarFormUsuario();
            });

            $('#tb_usuario').on('click', '.editar-user', function () {
                $("#title-modal-reg").html("Editar usuario");
                $("#mUsuario").modal("show");
                vUsuario.cargarDatosUsuario(this);
            });

            $('#tb_usuario').on('click', '.des-user', function () {

                vUsuario.deshabilitarUsuario(this);
            });

            $('#tb_usuario').on('click', '.hab-user', function () {

                vUsuario.habilitarUsuario(this);
            });

            $(".form-control").keypress(function (event) {
                vUsuario.enterTab(event, this);
            });
        },
        enterTab: function (event, vThis) {

            if (event.keyCode == 13) {
                $(vThis).parent().parent().next().find(".form-control").focus();
            }

        },
        validaRegistrar: function () {

            var vDni = $("#dni").val();
            var vApPat = $("#ap_pat").val();
            var vApMat = $("#ap_mat").val();
            var vNombres = $("#nombres").val();
            var vClave = $("#clave").val();
            var vDireccion = $("#dir_usu").val();
            var vFecNac = $("#fec_nac").val();
            var vId = $("#cod_usu").val();

            if (vFecNac === "") {
                vFecNac = "1/1/1890";
            }

            var vUsuarioReg = {
                Dni: vDni,
                Ap_Pat: vApPat,
                Ap_Mat: vApMat,
                NombreUsuario: vNombres,
                Password: vClave,
                Direccion: vDireccion,
                Fecha_Nac: vFecNac,
                Cod_Usu : vId
            };

            if (vDni == "" || vApMat == "" || vApPat == "" || vNombres == "" || vClave == "") {
                sweetAlert("Mensaje del sistema", "No se pudo registrar : Faltan datos", "error");
            } else {
                if ($("#title-modal-reg").html() == "Editar usuario") {
                    vUsuario.editarUsuario(vUsuarioReg);
                } else {
                    vUsuario.registrarUsuario(vUsuarioReg);
                }
               
            }
        },
        registrarUsuario: function(vUsuarioData) {

            var vData = {
                Usuario: JSON.stringify(vUsuarioData)
            };

            $.ajax({
                url: "/Usuario/Registrar",
                type: "POST",
                data: JSON.stringify(vData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (data == "0") {

                        sweetAlert("No se pudo registrar", "Revise los datos", "error");

                    } else {

                        $("#mUsuario").modal("hide");

                        sweetAlert("Operación exitosa", "Se registro el usuario", "success");

                        setTimeout(function () {
                            location.reload();
                        }, 2000);

                    }
                }
            });

        },
        alertaRegistro: function () {
            sweetAlert("Mensaje del sistema", "Usted no puede agregar más usuarios", "info");
        },
        cargarDatosUsuario: function (vThis) {
            var vId = $(vThis).parents('tr').find("td:eq(0)").html().trim();
            var vNombres = $(vThis).parents('tr').find("td:eq(9)").html().trim();
            var vApPat = $(vThis).parents('tr').find("td:eq(10)").html().trim();
            var vApMat = $(vThis).parents('tr').find("td:eq(11)").html().trim();
            var vDni = $(vThis).parents('tr').find("td:eq(2)").html().trim();
            var vClave = $(vThis).parents('tr').find("td:eq(8)").html().trim();
            var vDireccion = $(vThis).parents('tr').find("td:eq(6)").html().trim();
            var vFechaNac = $(vThis).parents('tr').find("td:eq(7)").html().trim();

            $("#nombres").val(vNombres);
            $("#ap_pat").val(vApPat);
            $("#ap_mat").val(vApMat);
            $("#dni").val(vDni);
            $("#clave").val(vClave);
            $("#dir_usu").val(vDireccion);
            $("#fec_nac").val(vFechaNac);
            $("#cod_usu").val(vId);
        },
        editarUsuario: function (vUsuarioData) {
            var vData = {
                Usuario: JSON.stringify(vUsuarioData)
            };

            $.ajax({
                url: "/Usuario/Editar",
                type: "POST",
                data: JSON.stringify(vData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (data == "0") {

                        sweetAlert("No se pudo editar", "Revise los datos", "error");

                    } else {

                        $("#mUsuario").modal("hide");
                        vUsuario.limpiarFormUsuario();
                        sweetAlert("Operación exitosa", "Se guardo los datos del usuario", "success");
                        setTimeout(function () {
                            location.reload();
                        }, 2000);

                    }
                }
            });

        },
        limpiarFormUsuario: function () {
            $("#dni").val("");
            $("#ap_pat").val("");
            $("#ap_mat").val("");
            $("#nombres").val("");
            $("#clave").val("");
            $("#dir_usu").val("");
            $("#fec_nac").val("");
            $("#cod_usu").val("");
        },
        deshabilitarUsuario: function (vThis) {
            var vId = $(vThis).parents('tr').find("td:eq(0)").html().trim();
            swal({
                title: "Confirmación",
                text: "¿Estas seguro que deseas deshabilitar este usuario?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Deshabilitar!",
                closeOnConfirm: true
            },
            function () {
                vUsuario.editarEstadoUsuario(vId, "0");

            });
        },
        habilitarUsuario: function (vThis) {
            var vId = $(vThis).parents('tr').find("td:eq(0)").html().trim();

            swal({
                title: "Confirmación",
                text: "¿Estas seguro que deseas habilitar este usuario?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Habilitar!",
                closeOnConfirm: true
            },
            function () {
                vUsuario.editarEstadoUsuario(vId,"1");

            });
        },
        editarEstadoUsuario: function (vCodUsu, vEstado) {

            var vUsuarioData = {
                Cod_Usu : vCodUsu,
                Estado : vEstado
            };
            var vData = {
                Usuario: JSON.stringify(vUsuarioData)
            };

            $.ajax({
                url: "/Usuario/EditarEstado",
                type: "POST",
                data: JSON.stringify(vData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    location.reload();
                }
            });
        }

    };
 

    vUsuario.init();
});