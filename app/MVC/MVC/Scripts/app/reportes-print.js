﻿$(function () {
    var vReporte = {

        init: function () {

            var vOpcion = $("#lblOpcion").html();

            if (vOpcion == "1") {
                $("#reporte-resumen").fadeIn(5);
                vReporte.verResumen();
            } else {
                $("#reporte-detalle").fadeIn(5);
                vReporte.verDetalle();
            }

            if ($("#lblTipo").html() == "1") {
                $("#lbl-r-venta").fadeIn(2);
                $("#lbl-d-venta").fadeIn(2);
            } else {
                $("#lbl-r-compra").fadeIn(2);
                $("#lbl-d-compra").fadeIn(2);
            }

        },
        verResumen: function () {

            var vParams = {
                fechaIni: $("#lblFechaIni").html(),
                fechaFin: $("#lblFechaFin").html(),
                tipoTran: $("#lblTipo").html(),
                ruc: $("#lblRuc").html(),
            };

            $.ajax({
                url: "/Reporte/BuscarDocumento",
                type: "POST",
                data: JSON.stringify(vParams),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var htmlTable = '';
                    var vCabecera = data;
                    var vTotal = 0;

                    if (vCabecera.length > 0) {

                        var vTipoTrans = "Venta";
                        $(vCabecera).each(function (item, data) {

                            item += 1;

                            vTotal += parseFloat(this.Total);

                            htmlTable += '<tr>';
                            htmlTable += '<td>' + this.Codigo + '</td>';
                            htmlTable += '<td>' + vTipoTrans + '</td>';
                            htmlTable += '<td>' + this.Fecha + '</td>';
                            htmlTable += '<td>' + this.Documento + '</td>';
                            htmlTable += '<td>' + this.Dni_Ruc + '</td>';
                            htmlTable += '<td>' + this.Cliente + '</td>';
                            htmlTable += '<td>' + this.Total + '</td>';
     
                            htmlTable += '</tr>';

                        });
                    }

                    $("#total-resumen").html("S/. " + vTotal.toFixed(2));

                    $("#tb_resumen").html(htmlTable);

                }
            });
        },
        verDetalle: function () {

            var vParams = { idCorrelativo: $("#lblIdCabecera").html() };

            $.ajax({
                url: "/Reporte/MostrarDetalleDocumento",
                type: "POST",
                data: JSON.stringify(vParams),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var htmlTable = '';
                    var vProductos = data;
                    var vTotal = 0;

                    $(vProductos).each(function (item, data) {

                        item += 1;
                        vTotal += parseFloat(this.Total);

                        htmlTable += '<tr>';
                        htmlTable += '<td>' + this.Cod_Prod + '</td>';
                        htmlTable += '<td>' + this.Descripcion + '</td>';
                        htmlTable += '<td>' + this.Unidad + '</td>';
                        htmlTable += '<td>' + this.Marca + '</td>';
                        htmlTable += '<td>' + this.Precio + '</td>';
                        htmlTable += '<td>' + this.Cantidad + '</td>';
                        htmlTable += '<td>' + this.Total + '</td>';

                        htmlTable += '</tr>';

                    });

                    $("#tb_detalle").html(htmlTable);
                    $("#total-detalle").html("S/. " + vTotal.toFixed(2));
                }
            });
        }
    };

    vReporte.init();
});