﻿$(function () {

    var vReportes = {
        init: function () {
            select_item_menu(4);

            $(".fecha").mask("99/99/9999");

            $("#btn-buscar").click(function () {
                vReportes.validaBuscar();
            });

            $(".input-sm").keypress(function (event) {
                vReportes.enterTab(event,this);
            });

            $("select").change(function (event) {
                $(this).parent().next().find(".input-sm").focus();
            });

            $(".btn-reporte").click(function (event) {
                event.preventDefault();
                var vUrl = $(this).attr("href");
                
                window.open(vUrl, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=780,height=700");
            });

            vReportes.verDetalleDocumento();  
        },
        enterTab: function (event,vThis) {

            if (event.keyCode == 13) {
                $(vThis).parent().next().find(".input-sm").focus();
            }

        },
        verDetalleDocumento : function(){
            $('#tb_cabecera').on('click', '.rdio.rdio-warning input', function () {

                var vId = $(this).parents('tr').find("td:eq(0)").html();

                var vParams = { idCorrelativo: vId };

                var vTransaccion = $("#tipo option:selected").val();

                $("#btn-detalle").removeAttr("disabled");
                $("#btn-detalle").attr("href", "/Reporte/Visualizar?opcion=2&tipo="+vTransaccion+"&id_cabecera="+vId+"");

                $.ajax({
                    url: "/Reporte/MostrarDetalleDocumento",
                    type: "POST",
                    data: JSON.stringify(vParams),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        var htmlTable = '';
                        var vProductos = data;
                        var vTotal = 0;

                        $(vProductos).each(function (item, data) {

                            item += 1;
                            vTotal += parseFloat(this.Total);

                            htmlTable += '<tr>';
                            htmlTable += '<td style="display:none">' + this.Cod_Prod + '</td>';
                            htmlTable += '<td>' + this.Descripcion + '</td>';
                            htmlTable += '<td>' + this.Unidad + '</td>';
                            htmlTable += '<td>' + this.Marca + '</td>';
                            htmlTable += '<td>' + this.Precio + '</td>';
                            htmlTable += '<td>' + this.Cantidad + '</td>';
                            htmlTable += '<td>' + this.Total + '</td>';
                   
                            htmlTable += '</tr>';

                        });

                        $("#tb_detalle").html(htmlTable);
                        $("#tot-prod").html(vProductos.length);
                        $("#tot-detalle").html(vTotal.toFixed(2));
                    }
                });

            });
        },
        validaBuscar: function () {

            var vTransaccion = $("#tipo option:selected").val();
            var vRuc = $("#ruc").val();
            var vFechaIni = change_format_fecha($("#fecha-ini").val());
            var vFechaFin = change_format_fecha($("#fecha-fin").val());

            var vParams = { fechaIni: vFechaIni, fechaFin: vFechaFin, tipoTran: vTransaccion, ruc: vRuc };

            if(vFechaFin == "" || vFechaIni == ""){
                sweetAlert("No se pudo realizar la busqueda", "Faltan datos", "error");
            } else {
                vReportes.buscarTransacciones(vParams);
            }

        },
        buscarTransacciones: function (vParams) {
          
            $.ajax({
                url: "/Reporte/BuscarDocumento",
                type: "POST",
                data: JSON.stringify(vParams),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var htmlTable = '';
                    var vCabecera = data;
                    var vTotal = 0;

                    $("#tb_detalle").html("");

                    if (vCabecera.length > 0) {

                        var vTipoTrans = $("#tipo option:selected").text();
                        $("#btn-resumen").removeAttr("disabled");
                        var vUrlReporte = "/Reporte/Visualizar/?opcion=1&fecha_ini="+vParams.fechaIni+"&fecha_fin="+vParams.fechaFin+"&tipo="+vParams.tipoTran+"&ruc="+vParams.ruc+"";
                        $("#btn-resumen").attr("href", vUrlReporte);
                        $(vCabecera).each(function (item, data) {

                            item += 1;

                            vTotal += parseFloat(this.Total);

                            htmlTable += '<tr>';
                            htmlTable += '<td style="display:none">' + this.Codigo + '</td>';
                            htmlTable += '<td style="display:none">' + vTipoTrans + '</td>';
                            htmlTable += '<td style="display:none">' + this.Fecha + '</td>';
                            htmlTable += '<td>' + this.Documento + '</td>';
                            htmlTable += '<td>' + this.Dni_Ruc + '</td>';
                            htmlTable += '<td>' + this.Cliente + '</td>';
                            htmlTable += '<td>' + this.Total + '</td>';
                            htmlTable += '<td>  <div class="rdio rdio-warning">' +
                                         '<input type="radio" name="radio" id="radioDefault' + item + '" value="1" />' +
                                         '<label for="radioDefault' + item + '"></label>' +
                                         '</div></td>';
                            htmlTable += '</tr>';

                        });

                        $("#total").html(vTotal.toFixed(2));

                    } else {
                        $("#btn-resumen").attr("disabled", "true");
                        $("#btn-detalle").attr("disabled", "true");
                        $("#total").html("");

                        $("#tb_detalle").html("");
                        $("#tot-prod").html("");
                        $("#tot-detalle").html("");
                        sweetAlert("No hay registros", "Tu busqueda no obtuvo resultados", "info");
                        
                    }
                   
                    $("#tb_cabecera").html(htmlTable);
           
                }
            });
        }

    };

    vReportes.init();
});