﻿$(function () {

    var vItemProd = 1;
    var vProdList = [];
    var vTotalVenta = 0.00;
    var vCompras= {
        init: function () {
            select_item_menu(2);

            $("#fecha").mask("99/99/9999");

            vCompras.buscarProducto("");

            $(".numero").keydown(function (event) {
                vCompras.enterTab(event, this);
                solo_numeros(event);
            });

            $("#producto").keyup(function () {
                vCompras.buscarProducto($(this).val());
            });

            $('#tb_producto').on('click', '.item-prod', function () {
                vCompras.agregarProducto(this);
            });

            $("#btn-registrar").click(function () {
                vCompras.validaRegistrar();
            });

            $(".input-sm").keypress(function (event) {
                vCompras.enterTab(event, this);
            });

            $("select").change(function (event) {
                $(this).parent().parent().next().find(".input-sm").focus();
            });


            vCompras.deleteProducto();

        },
        enterTab: function (event, vThis) {

            if (event.keyCode == 13) {
                $(vThis).parent().parent().next().find(".input-sm").focus();
            }

        },
        buscarProducto: function (vParam) {

            var actionData = { texto: vParam };

            $.ajax({
                url: "/Venta/BuscarProducto",
                type: "POST",
                data: JSON.stringify(actionData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var producto = data;
                    var htmlTable = '';

                    $(producto).each(function (item, data) {

                        item += 1;

                        htmlTable += '<tr>';

                        htmlTable += '<td style="display:none">' + item + '</td>';
                        htmlTable += '<td style="display:none">' + this.Codigo + '</td>';
                        htmlTable += '<td>' + this.Descripcion + '</td>';
                        htmlTable += '<td>' + this.Unidad + '</td>';
                        htmlTable += '<td>' + this.Marca + '</td>';
                        htmlTable += '<td>' + this.Precio + '</td>';
                        htmlTable += '<td class="table-action"> ' +
                        '<a href="javascript:void(0)" data-toggle="tooltip" title="Agregar" class="tooltips item-prod"><i class="fa fa-plus-square-o"></i></a>' +
                        '</td>';

                        htmlTable += '</tr>';


                    });

                    $("#tb_producto").html(htmlTable);


                }
            });

        },
        agregarProducto: function (vThis) {

            var vIdProd = $(vThis).parents('tr').find("td:eq(1)").html();
            var vProd = $(vThis).parents('tr').find("td:eq(2)").html();
            var vUnidad = $(vThis).parents('tr').find("td:eq(3)").html();
            var vMarca = $(vThis).parents('tr').find("td:eq(4)").html();
            var vPrecio = parseFloat($(vThis).parents('tr').find("td:eq(5)").html());

            var vCantidad = 1;
            var vTotal = vPrecio * vCantidad;

            var tableHtml = '<tr>';
            tableHtml += '<td style="display:none">' + vItemProd + '</td>';
            tableHtml += '<td style="display:none">' + vIdProd + '</td>';
            tableHtml += '<td>' + vProd + '</td>';
            tableHtml += '<td>' + vUnidad + '</td>';
            tableHtml += '<td>' + vMarca + '</td>';
            tableHtml += '<td>' + vPrecio.toFixed(2) + '</td>';
            tableHtml += '<td><a href="#" class="cant" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Cantidad">' + vCantidad + '</a></td>';
            tableHtml += '<td>' + vTotal.toFixed(2) + '</td>';
            tableHtml += '<td class="table-action">' +
            ' <a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="delete-row tooltips"><i class="fa fa-trash-o"></i></a>' +
            '</td>';
            tableHtml += '</tr>';

            if (vProdList.indexOf(vIdProd) == -1) {

                $("#tb_producto_venta").append(tableHtml);

                vProdList.push(vIdProd);

                vItemProd += 1;
  
                vCompras.initControlTableProd();

            } else {

                $("#tb_producto_venta tr").each(function (index) {

                    if ($(this).find("td:eq(1)").html() == vIdProd) {

                        vCantidad = parseInt($(this).find("td:eq(6) a").html()) + 1;
                        vTotal = vCantidad * parseFloat($(this).find("td:eq(5)").html());

                        $(this).find("td:eq(6) a").html(vCantidad);
                        $(this).find("td:eq(7)").html(vTotal.toFixed(2));
                    }

                });

            }
            vCompras.calculaTotal();

        },
        initControlTableProd: function () {

            $('.cant').editable({
                clear: false,
                validate: function (value) {
                    if ($.trim(value) === '' || $.trim(value) === '0') return 'Este campo es requerido';
                    setTimeout(function () {
                        vCompras.calculaTotal();
                    }, 1000);
                }
            });

        },
        calculaTotal: function () {

            vTotalVenta = 0.00;
            var vCantidadTotal = 0;

            $("#tb_producto_venta tr").each(function (index) {

                var vCantidad = parseInt($(this).find("td:eq(6) a").html());
                var vTotal = vCantidad * parseFloat($(this).find("td:eq(5)").html());

                $(this).find("td:eq(6) a").html(vCantidad);
                $(this).find("td:eq(7)").html(vTotal.toFixed(2));

                vTotalVenta += vTotal;
                vCantidadTotal += vCantidad;

            });

            vTotalVenta = vTotalVenta.toFixed(2)

            $("#tot-venta").html("S/. " + vTotalVenta);

            $("#tot-prod").html(vCantidadTotal);


        },
        deleteProducto: function () {

            $('#tb_producto_venta').on('click', '.delete-row', function () {

                var vIdProd = $(this).parents('tr').find("td:eq(1)").html();

                var vIndex = vProdList.indexOf(vIdProd);

                vProdList.splice(vIndex, 1);

                $(this).parents('tr').remove();

                vCompras.calculaTotal();
                vItemProd -= 1;

                vCompras.updateIndexProdVentas();

            });
        },
        updateIndexProdVentas: function () {

            $("#tb_producto_venta tr").each(function (index) {
                console.log(index);
                $(this).find("td:eq(0)").html(index + 1);
            });

        },
        registrarCabecera: function () {

            var vTotalVenta = $("#tot-venta").html();
            vTotalVenta = vTotalVenta.replace("S/. ", "");

            var vFecha = change_format_fecha($("#fecha").val());

            var vCabecera = {
                Fecha: vFecha,
                Documento: $("#documento option:selected").val(),
                Serie: $("#serie").val(),
                Dni_Ruc: $("#ruc").val(),
                Cliente: $("#cliente").val(),
                Cant_Prod: $("#tot-prod").html(),
                Total: vTotalVenta,
                Numero: $("#numero").val(),
                Tipo_Tran: "1"
            };

            var vData = {
                Cabecera: JSON.stringify(vCabecera)
            };

            vCompras.datosCabeceraTicket();

            $.ajax({
                url: "/Compra/RegistrarCabecera",
                type: "POST",
                data: JSON.stringify(vData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (data == "01") {

                        sweetAlert("Documento no se pudo registra", "Documento ya existe", "error");

                    } else {

                        vCompras.registrarDetalle(data);
                    }

                }
            });

        },
        registrarDetalle: function (idCabecera) {

            $("#tb_producto_venta tr").each(function (index) {

                var vCantidad = parseInt($(this).find("td:eq(6) a").html());
                var vTotal = parseFloat($(this).find("td:eq(7)").html());
                var vPrecio = parseFloat($(this).find("td:eq(5)").html());
                var vIdProd =$(this).find("td:eq(1)").html();

                var vDetalle = {
                    Id: idCabecera,
                    Cod_Prod: vIdProd,
                    Precio: vPrecio,
                    Cantidad: vCantidad,
                    Total: vTotal,
                    Tipo_Tran: "1"
                };

                var vData = {
                    Detalle: JSON.stringify(vDetalle)
                };

                $.ajax({
                    url: "/Compra/RegistrarDetalle",
                    type: "POST",
                    data: JSON.stringify(vData),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                    }
                });

            });

            swal({
                title: "Registro exitoso",
                text: "Documento Registrado Correctamente",
                type: "success",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000,
            });

            vCompras.datosDetalleTicket(idCabecera);

        },
        validaRegistrar: function () {
            var vSerie = $("#serie").val();
            var vNumero = $("#numero").val();
            var vRuc = $("#ruc").val();
            var vCliente = $("#cliente").val();
            var vFecha = $("#fecha").val();

            var vProductos = $("#tb_producto_venta tr").length;

            if (vSerie == "" || vNumero == "" || vRuc == "" || vCliente == "" || vFecha == "" || vProductos == 0) {

                sweetAlert("No se pudo registrar", "Faltan ingresar datos", "error");

            } else {
                vCompras.registrarCabecera();
            }
        },
        limpiarForm: function () {

            $("#serie").val("");
            $("#numero").val("");
            $("#ruc").val("");
            $("#cliente").val("");
            $("#fecha").val("");
            $("#producto").val("");
            $("#tot-venta").html("S/. 00.00");
            $("#tot-prod").html("0");

            vCompras.buscarProducto("");
            vItemProd = 1;
            console.log("total prodi " + vProdList.length);
            console.log("productos id " + JSON.stringify(vProdList));
            $("#tb_producto_venta tr").each(function (index) {
                $(this).remove();
            });
            vProdList.length = 0;

            console.log("productos id actual " + JSON.stringify(vProdList));
        },
        datosCabeceraTicket: function () {

            var vDocumento = $("#documento option:selected").text() + " - " + $("#serie").val() + " - " + $("#numero").val();

            $("#lbl-cliente").html($("#cliente").val());
            $("#lbl-ruc").html($("#ruc").val());
            $("#lbl-fecha").html($("#fecha").val());
            $("#lbl-documento").html(vDocumento);
        },
        datosDetalleTicket: function (idCabecera) {
            var vItem = 1;
            var tableHtml = '';

            $("#lbl-venta").html("Compra (" + idCabecera + ")");

            $("#tb_producto_venta tr").each(function (index) {

                var vCantidad = parseInt($(this).find("td:eq(6) a").html());
                var vTotal = parseFloat($(this).find("td:eq(7)").html());
                var vPrecio = parseFloat($(this).find("td:eq(5)").html());
                var vProd = $(this).find("td:eq(2)").html();

                tableHtml += '<tr>';

                tableHtml += '<td>' + vItem + '</td>';
                tableHtml += '<td>' + vProd + '</td>';
                tableHtml += '<td style="text-align:center">' + vCantidad + '</td>';
                tableHtml += '<td style="text-align:center">' + vPrecio.toFixed(2) + '</td>';
                tableHtml += '<td style="text-align:center">' + vTotal.toFixed(2) + '</td>';

                tableHtml += '</tr>';

                vItem += 1;

            });

            $("#tb_detalle_ticket").html(tableHtml);
            $("#lbl-total-detalle").html($("#tot-venta").html());

            setTimeout(function () {

                swal({
                    title: "Ticket",
                    text: "¿Deseas imprimir el comprobante?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (isConfirm) {

                    if (isConfirm) {
                        vCompras.imprimirTicket();
                    }

                    vCompras.limpiarForm();

                });

            }, 3000);
        },
        imprimirTicket: function () {
            $('#reporte-ticket').fadeIn(5);

            setTimeout(function () {
                $('#reporte-ticket').printArea();
                setTimeout(function () {
                    $('#reporte-ticket').fadeOut(5);
                }, 1000);
            }, 1000);

        },



    };

    vCompras.init();
});