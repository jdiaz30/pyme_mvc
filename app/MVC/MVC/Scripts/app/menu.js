﻿$(function () {

    var vMenu = {
        init: function () {

            $("#logout").click(function () {
                vMenu.logout();
            });

            $("#chat-trust").click(function () {
                vMenu.closeChat();
            });

            $("#close-chat").click(function () {
                vMenu.closeChat();
            });

        },

        logout: function () {
            $.ajax({
                url: "/Login/Logout",
                type: "POST",
                data: {},
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    location.reload();
                }
            });

        },
        closeChat: function () {
            if ($("#chat-box").hasClass("fadeout"))
                $("#chat-box").removeClass("fadeout").addClass("fadein");
            else
                $("#chat-box").removeClass("fadein").addClass("fadeout");
        }

    };

    vMenu.init();
});