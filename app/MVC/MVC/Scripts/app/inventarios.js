﻿$(function () {

    var vInventario = {
        init: function () {
            select_item_menu(3);

            vInventario.buscarProducto("");
            //vInventario.resetGrupo();
  
            $(".money").maskMoney();

            $("#grupo").change(function () {
                vInventario.getSubGrupo();
            });


            $("#stock").keydown(function (event) {
                solo_numeros(event);
            });

            $("#btn-registrar").click(function () {
                vInventario.validaRegistrar();
            });

            $("#btn-login").click(function () {
                vInventario.login();
            });

            $("#producto").keyup(function () {
                vInventario.buscarProducto($(this).val());
            });

            $(".input-sm").keypress(function (event) {
                vInventario.enterTab(event, this);
            });


            vInventario.getSubGrupo();
        },
        enterTab: function (event, vThis) {

            if (event.keyCode == 13) {
                $(vThis).parent().parent().next().find(".input-sm").focus();
            }

        },
        buscarProducto: function (vParam) {

            var actionData = { texto: vParam };

            $.ajax({
                url: "/Venta/BuscarProducto",
                type: "POST",
                data: JSON.stringify(actionData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var producto = data;
                    var htmlTable = '';

                    $(producto).each(function (item, data) {

                        item += 1;

                        var vPrecio = parseFloat(this.Precio);
                        var vCosto = parseFloat(this.Precio_Com);
                        //var vMargen = ((vPrecio - vCosto) / 100) * 100;
                        var vMargen = ((vPrecio / vCosto) - 1) * 100;
                        vMargen = vMargen.toFixed(2);

                        htmlTable += '<tr>';

                        htmlTable += '<td style="display:none">' + item + '</td>';
                        htmlTable += '<td style="display:none">' + this.Codigo + '</td>';
                        htmlTable += '<td style="text-align:left;">' + this.Descripcion + '</td>';
                        htmlTable += '<td>' + this.Unidad + '</td>';
                        htmlTable += '<td>' + this.Marca + '</td>';
                        htmlTable += '<td><a href="#" class="precio" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Precio compra">' + this.Precio_Com + '</a></td>';
                        htmlTable += '<td><a href="#" class="precio" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Precio venta">' + this.Precio + '</a></td>';
                        htmlTable += '<td>' + this.Stock + '</td>';
                        htmlTable += '<td>' + vMargen + '</td>';
                        htmlTable += '</tr>';


                    });

                    $("#tb_producto").html(htmlTable);
                    vInventario.initControlTableProd();

                }
            });

        },
        initControlTableProd: function () {

            $('.precio').editable({
                clear: false,
                validate: function (value) {
                    if ($.trim(value) === '' || $.trim(value) === '0') return 'Este campo es requerido';

                    if($("#tipo_user").val() == "U"){
                        
                        swal({
                            title: "Usted no puede editar precios",
                            text: "¿Inicia sesion como administrador?",
                            type: "info",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Si",
                            cancelButtonText: "No",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        }, function (isConfirm) {

                            if (isConfirm) {
                                $("#mLogin").modal("show");
                            }

                        });
                    }else{
                        setTimeout(function () {
                            vInventario.editarPrecios();
                        }, 1000);
                    }
                  

                }
            });

            $('.precio').on('shown', function (e, editable) {
                editable.input.$input.maskMoney();

            });
        },
        resetGrupo: function () {
            $("#grupo option:eq(0)").remove();
        },
        getSubGrupo: function () {

            var vRubroGrupo = $("#grupo option:selected").val();

            var actionData = { rubro: vRubroGrupo };

            $.ajax({
                url: "/Inventario/SubGrupoComercial",
                type: "POST",
                data: JSON.stringify(actionData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $(data).each(function (item, data) {
                        $('#sub_grupo').html("");
                        $('#sub_grupo')
                        .html($("<option></option>")
                        .attr("value", this.Codigo)
                        .text(this.Descripcion));
                    });
                }
            });

        },
        registrarProducto: function () {

            var vRubroGrupo = $("#grupo option:selected").val();
            var vSubGrupo = $("#sub_grupo option:selected").val();
            var vPrecio = $("#venta").val().replace(",","");
            var vPrecioCompra = $("#costo").val().replace(",", "");
            var vProducto = $("#descripcion").val();
            var vMarca = $("#marca").val();
            var vStock = $("#stock").val();
            var vUnidad = $("#unidad option:selected").val();

            var vProducto = {
                Rubro_Com: vRubroGrupo,
                Grupo: vSubGrupo,
                Precio: vPrecio,
                Descripcion: vProducto,
                Marca: vMarca,
                Stock: vStock,
                Unidad: vUnidad,
                Precio_Com : vPrecioCompra
            };

            var vData = {
                Producto : JSON.stringify(vProducto)
            };

            $.ajax({
                url: "/Inventario/RegistrarProducto",
                type: "POST",
                data: JSON.stringify(vData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (data == "0") {
                        sweetAlert("Producto no registrado", "Verifique los datos", "error");
                    } else {
                        sweetAlert("Operación exitosa", "Producto registrado", "success");

                        $("#mProducto").modal("hide");
                        vInventario.limpiarForm();
                        vInventario.buscarProducto("");
                    }
                }
            });

        },
        validaRegistrar: function () {

            var vPrecio = $("#venta").val();
            var vPrecioCompra = $("#costo").val();
            var vProducto = $("#descripcion").val();
            var vMarca = $("#marca").val();
            var vStock = $("#stock").val();

            if (vPrecio == "0.00" || vPrecio == "" || vPrecioCompra == "0.00" || vPrecioCompra == "" || vProducto == "" || vMarca == "" || vStock == "") {
                sweetAlert("Producto no registrado", "Faltan datos en el formulario", "error");
            } else {
                vInventario.registrarProducto();
            }

        },
        editarPrecios: function () {
            $("#tb_producto tr").each(function (index) {
                var vPrecVenta = parseFloat($(this).find("td:eq(6) a").html());
                var vPrecCompra = parseFloat($(this).find("td:eq(5) a").html());
                var vIdProd = parseInt($(this).find("td:eq(1)").html());

                var vData = {
                    codProd: vIdProd,
                    precioCom: vPrecCompra,
                    precioVen: vPrecVenta,
                }

                $.ajax({
                    url: "/Inventario/EditarPrecio",
                    type: "POST",
                    data: JSON.stringify(vData),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                    }
                });
            });

            setTimeout(function () {
                vInventario.buscarProducto("");
            }, 1000);
            
            sweetAlert("Registro exitoso", "Se editaron los datos satisfactoriamente", "success");
   
        },
        limpiarForm: function () {
            $(".modal-body.panel-form input[type='text']").val("");
        },
        login: function () {
            var vUsuario = {
                Dni: $("#dni").val(),
                Password: $("#pass").val()
            };

            var vData = {
                Usuario: JSON.stringify(vUsuario)
            };
            $.ajax({
                url: "/Inventario/Logeo",
                type: "POST",
                data: JSON.stringify(vData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var usuario = data;
                    if (usuario.length > 0) {

                        var vTipoUser = "";

                        $(usuario).each(function () {
                            vTipoUser = this.Tipo;
                        });

                        if (vTipoUser == "A") {

                            $("#mLogin").modal("hide");

                            vInventario.editarPrecios();

                        } else {
                            swal({
                                title: "Mensaje del sistema",
                                text: "Necesitas credenciales de administrador!",
                                type: "info",
                                showCancelButton: false,
                                showConfirmButton: false,
                                timer: 2000,
                            });
                        }
              

                    } else {
                        sweetAlert("No se pudo ingresar", "Sus credenciales no son correctas", "error");
                    }

                }
            });
        }
       
    };

    vInventario.init();
});