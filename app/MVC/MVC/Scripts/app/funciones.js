﻿function select_item_menu(vItem) {

    $(".nav.nav-pills.nav-stacked").find('.active').removeClass('active');

    $(".nav.nav-pills.nav-stacked li:eq("+vItem+")").addClass("active");

}

function solo_numeros(event) {

    if (event.shiftKey) {
        event.preventDefault();
    }

    if (event.keyCode == 46 || event.keyCode == 8) {
    }
    else {
        if (event.keyCode < 95) {
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.preventDefault();
            }
        }
        else {
            if (event.keyCode < 96 || event.keyCode > 105) {
                event.preventDefault();
            }
        }
    }
}

function format_fecha() {

    var vDate = new Date();

    var vAnio = vDate.getFullYear();
    var vDia = vDate.getDay();
    var vMes = vDate.getMonth() + 1;

    var vFecha = vDia + "/" + vMes + "/" + vAnio;

    return vFecha;

}

function change_format_fecha(vFecha) {

    var vDia = vFecha.substr(0,2);
    var vMes = vFecha.substr(3,2);
    var vAnio = vFecha.substr(6, 4);

    var vNewFecha = vAnio + "-" + vMes + "-" + vDia;

    return vNewFecha;
}