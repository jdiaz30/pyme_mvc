﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MVC.Models;
using MVC.App_Start;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace MVC.Repository
{
    public class ProductoRep
    {
        public List<ProductoModel> Filtrar(string texto)
        {
            List<ProductoModel> ProductoList = new List<ProductoModel>();
            ProductoModel mProducto = null;

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@empresa", HttpContext.Current.Session["cod_empresa"]));

                if(texto != ""){
                    parametros.Add(new SqlParameter("@tt", 1));
                    parametros.Add(new SqlParameter("@descripcion", texto.Trim()));
                }
                else
                {
                    parametros.Add(new SqlParameter("@tt", 2));

                }

                DataTable dtProducto = new Conexion().na_ExecuteDataTable("Usp_Buscar_Producto", parametros);

                if ((dtProducto.Rows.Count > 0))
                {
                    foreach (DataRow row in dtProducto.Rows)
                    {
                        mProducto = new ProductoModel();

                        mProducto.Codigo = (row["Codigo_Pro"] != DBNull.Value ? row["Codigo_Pro"].ToString() : "");
                        mProducto.Descripcion = (row["Producto"] != DBNull.Value ? row["Producto"].ToString() : "");
                        mProducto.Unidad = (row["Unidad"] != DBNull.Value ? row["Unidad"].ToString() : "");
                        mProducto.Precio = (row["Precio"] != DBNull.Value ? row["Precio"].ToString() : "");
                        mProducto.Precio_Com = (row["Costo"] != DBNull.Value ? row["Costo"].ToString() : "");
                        mProducto.Marca = (row["Marca"] != DBNull.Value ? row["Marca"].ToString() : "");
                        mProducto.Stock = (row["Stock"] != DBNull.Value ? row["Stock"].ToString() : "");

                        ProductoList.Add(mProducto);
                    }

                }

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return ProductoList;
        }
        public string Registrar(ProductoModel producto)
        {
            string codigo = "";
        
            try
            {
                string codigoProd = GenerarCodigo(producto.Grupo, HttpContext.Current.Session["cod_empresa"].ToString());
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@cod_empresa", HttpContext.Current.Session["cod_empresa"]));
                parametros.Add(new SqlParameter("@rubro_com", producto.Rubro_Com));
                parametros.Add(new SqlParameter("@grupo", producto.Grupo));
                parametros.Add(new SqlParameter("@precio", Convert.ToDouble(producto.Precio)));
                parametros.Add(new SqlParameter("@prec_compra", Convert.ToDouble(producto.Precio_Com)));
                parametros.Add(new SqlParameter("@producto", producto.Descripcion));
                parametros.Add(new SqlParameter("@marca", producto.Marca));
                parametros.Add(new SqlParameter("@stock", producto.Stock));
                parametros.Add(new SqlParameter("@unidad", producto.Unidad));
                parametros.Add(new SqlParameter("@codigo_prod", codigoProd));

                codigo = new Conexion().na_ExecuteScalar("Usp_Insertar_Producto", parametros).ToString();

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return codigo;
        }
        public string GenerarCodigo(string grupo, string empresa)
        {
            string codigo = "";
            string maxCodigo = "";
            var codigoGen = 0;

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@empresa", empresa));
                parametros.Add(new SqlParameter("@grupo", grupo));

                DataTable dtProducto = new Conexion().na_ExecuteDataTable("Usp_Codigo_Producto", parametros);

                maxCodigo = dtProducto.Rows[0]["Codigo"].ToString();

                if(maxCodigo != "" ){
                    maxCodigo = maxCodigo.Substring(maxCodigo.Length - 4);

                    codigoGen = Convert.ToInt32(maxCodigo) + 1;

                    if ((codigoGen <= 9))
                    {
                        codigo = "000" + codigoGen;
                    }
                    else if ((codigoGen > 9 & codigoGen <= 99))
                    {
                        codigo = "00" + codigoGen;
                    }
                    else if ((codigoGen > 99 & codigoGen <= 999))
                    {
                        codigo = "0" + codigoGen;
                    }
                    else if ((codigoGen > 999 & codigoGen <= 9999))
                    {
                        codigo = codigoGen.ToString();
                    }

                    codigo = empresa + grupo + codigo;

                }
                else
                {
                    codigo = empresa + grupo + "0001";
                }

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return codigo;
        }
        public string EditarPrecio(double precioCom ,double precioVen , string codProd)
        {
            string response = "0";

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@prec_vent", precioVen));
                parametros.Add(new SqlParameter("@prec_comp", precioCom));
                parametros.Add(new SqlParameter("@id_prod", codProd));

                new Conexion().na_ExecuteScalar("Usp_Editar_Producto_Precios", parametros);

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return response;
        }
    }
}