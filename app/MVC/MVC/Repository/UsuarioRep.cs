﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MVC.Models;
using MVC.App_Start;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace MVC.Repository
{
    public class UsuarioRep
    {
        public List<UsuarioModel> Login(UsuarioModel usuario,Boolean isSession = false)
        {
            List<UsuarioModel> usuarioList = new List<UsuarioModel>();

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@tt", 3));
                parametros.Add(new SqlParameter("@RUC_Empresa", usuario.Ruc));
                parametros.Add(new SqlParameter("@DNI_Usuario", usuario.Dni));
                parametros.Add(new SqlParameter("@Clave_Acceso", usuario.Password));

                DataTable dtUsuario = new Conexion().na_ExecuteDataTable("Usp_Acceso", parametros);

                if (dtUsuario.Rows.Count > 0)
                {
                   
                    foreach (DataRow row in dtUsuario.Rows)
                    {
                        if(isSession == true){

                            HttpContext.Current.Session["id_usuario"] = row["Codigo_Usuario"].ToString().Trim();
                            HttpContext.Current.Session["nombre_usuario"] = row["Nombres"].ToString().Trim();
                            HttpContext.Current.Session["empresa"] = row["Nombre_Empresa"].ToString().Trim();
                            HttpContext.Current.Session["cod_empresa"] = row["Codigo_Empresa"].ToString().Trim();
                            HttpContext.Current.Session["rubro_com"] = row["Codigo_Rubro_Comercial"].ToString().Trim();
                            HttpContext.Current.Session["tipo_user"] = row["tipo"].ToString().Trim();
                            HttpContext.Current.Session["licencia"] = row["licencia"].ToString().Trim();
                            HttpContext.Current.Session["dir_empresa"] = row["Direccion"].ToString().Trim();
                            HttpContext.Current.Session["ruc"] = row["Ruc"].ToString().Trim();
                        }

                        usuarioList.Add(new UsuarioModel
                        {
                            Empresa = row["Nombre_Empresa"].ToString().Trim(),
                            NombreUsuario = row["Nombres"].ToString().Trim() + " " + row["Apellido_Paterno"].ToString().Trim() + " " + row["Apellido_Materno"].ToString().Trim(),
                            Tipo = row["tipo"].ToString().Trim()
                        });

                    }

                }
            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return usuarioList;

        }
        public string Registrar(UsuarioModel usuario)
        {
            var codUsuario = "0";
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@ap_mat", usuario.Ap_Mat));
                parametros.Add(new SqlParameter("@ap_pat", usuario.Ap_Pat));
                parametros.Add(new SqlParameter("@nombres", usuario.NombreUsuario));
                parametros.Add(new SqlParameter("@dni", usuario.Dni));
                parametros.Add(new SqlParameter("@clave", usuario.Password));
                parametros.Add(new SqlParameter("@direccion", usuario.Direccion));
                parametros.Add(new SqlParameter("@fecha_nac", usuario.Fecha_Nac));
                parametros.Add(new SqlParameter("@tipo", usuario.Tipo));
                parametros.Add(new SqlParameter("@empresa", usuario.Codigo_Empresa));

                codUsuario = new Conexion().na_ExecuteScalar("Usp_Insertar_Usuario", parametros).ToString();

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }
            return codUsuario;
        }
        public string Editar(UsuarioModel usuario)
        {
            var response = "0";
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@tt", 1));
                parametros.Add(new SqlParameter("@ap_mat", usuario.Ap_Mat));
                parametros.Add(new SqlParameter("@ap_pat", usuario.Ap_Pat));
                parametros.Add(new SqlParameter("@nombres", usuario.NombreUsuario));
                parametros.Add(new SqlParameter("@dni", usuario.Dni));
                parametros.Add(new SqlParameter("@clave", usuario.Password));
                parametros.Add(new SqlParameter("@direccion", usuario.Direccion));
                parametros.Add(new SqlParameter("@fecha_nac", usuario.Fecha_Nac));
                parametros.Add(new SqlParameter("@cod_usu", usuario.Cod_Usu));

                new Conexion().na_ExecuteNonQuery("Usp_Editar_Usuario", parametros).ToString();

                response = "1";
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
                
            }
            return response;
        }
        public string EditarEstado(UsuarioModel usuario)
        {
            var response = "0";
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@tt", 2));
                parametros.Add(new SqlParameter("@estado", usuario.Estado));
                parametros.Add(new SqlParameter("@cod_usu", usuario.Cod_Usu));

                new Conexion().na_ExecuteNonQuery("Usp_Editar_Usuario", parametros).ToString();

                response = "1";
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);

            }
            return response;
        }
        public List<UsuarioModel> Listar(string empresa)
        {
            List<UsuarioModel> usuarioList = new List<UsuarioModel>();
            UsuarioModel mUsuario = null;

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(new SqlParameter("@empresa", empresa));
    
                DataTable dtUsuario = new Conexion().na_ExecuteDataTable("Usp_Listar_Usuario", parametros);

                foreach (DataRow row in dtUsuario.Rows)
                {
                   mUsuario = new UsuarioModel();

                   mUsuario.NombreUsuario = row["Nombres"].ToString().Trim();
                   mUsuario.Ap_Pat = (row["Apellido_Paterno"] != DBNull.Value ? row["Apellido_Paterno"].ToString() : "");
                   mUsuario.Ap_Mat = (row["Apellido_Materno"] != DBNull.Value ? row["Apellido_Materno"].ToString() : "");
                   mUsuario.Estado = (row["estado"] != DBNull.Value ? row["estado"].ToString() : "0");
                   mUsuario.Dni = (row["DNI"] != DBNull.Value ? row["DNI"].ToString() : "");
                   mUsuario.Cod_Usu = (row["Codigo_Usuario"] != DBNull.Value ? row["Codigo_Usuario"].ToString() : "");
                   mUsuario.Tipo = (row["tipo"] != DBNull.Value ? row["tipo"].ToString() : "");
                   mUsuario.Estado = (row["estado"] != DBNull.Value ? row["estado"].ToString() : "0");
                   mUsuario.Fecha_Nac = (row["fecha_nac"] != DBNull.Value ? row["fecha_nac"].ToString() : "");
                   mUsuario.Direccion = (row["direccion"] != DBNull.Value ? row["direccion"].ToString() : "");
                   mUsuario.Password = (row["Clave_Acceso"] != DBNull.Value ? row["Clave_Acceso"].ToString() : "");

                   usuarioList.Add(mUsuario);

                }


            }catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return usuarioList;

        }
    }
}