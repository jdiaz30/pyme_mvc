﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MVC.Models;
using MVC.App_Start;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace MVC.Repository
{
    public class EmpresaRep
    {
        public string CodigoEmpresa()
        {
            var codigo = "";
            var maxCodigo = 0;
            var codigoInt = 0;

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@tt", 1));

                DataTable dtEmpresa = new Conexion().na_ExecuteDataTable("Usp_Codigo_Empresa", parametros);

                if (dtEmpresa.Rows[0]["Codigo"] != DBNull.Value)
                {
                    maxCodigo = Convert.ToInt32(dtEmpresa.Rows[0]["Codigo"]);
                }

                if (maxCodigo != 0)
                {
                    codigo = (maxCodigo + 1).ToString();
                    codigoInt = Int32.Parse(codigo);

                    if ((codigoInt <= 9))
                    {
                        codigo = "000" + codigo;
                    }
                    else if ((codigoInt > 9 & codigoInt <= 99))
                    {
                        codigo = "00" + codigo;
                    }
                    else if ((codigoInt > 99 & codigoInt <= 999))
                    {
                        codigo = "0" + codigo;
                    }
                    else if ((codigoInt > 999 & codigoInt <= 9999))
                    {
                        codigo = codigoInt.ToString();
                    }

                }
                else
                {
                    codigo = "0001";

                }

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return codigo;
        }

        public int Registrar(EmpresaModel empresa)
        {
            var codigo = 0;
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@rubro_com", empresa.Cod_Rub_Com));
                parametros.Add(new SqlParameter("@ruc", empresa.Ruc));
                parametros.Add(new SqlParameter("@razon_social", empresa.Razon_Social));
                parametros.Add(new SqlParameter("@rep_legal", empresa.Rep_Legal));
                parametros.Add(new SqlParameter("@ubicacion_geo", empresa.Ubi_Geo));
                parametros.Add(new SqlParameter("@direccion", empresa.Direccion));
                parametros.Add(new SqlParameter("@codigo_empresa", empresa.Cod_Empresa));

                new Conexion().na_ExecuteScalar("Usp_Insertar_Empresa", parametros);

                codigo = 1;

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }
            return codigo;

        }
    }
}