﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MVC.Models;
using MVC.App_Start;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace MVC.Repository
{
    public class UbiGeoRep
    {
        public List<UbiGeoModel> ListarDepartamento()
        {
            List<UbiGeoModel> UbiGeoList = new List<UbiGeoModel>();
            UbiGeoModel mUbiGeo = null;

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(new SqlParameter("@tt", 1));

                DataTable dtUbicacion = new Conexion().na_ExecuteDataTable("Usp_Ubicacion_Geografica", parametros);

                if ((dtUbicacion.Rows.Count > 0))
                {
                    foreach (DataRow row in dtUbicacion.Rows)
                    {
                        mUbiGeo = new UbiGeoModel();
                        
                        mUbiGeo.Id = row["Codigo_Departamento"].ToString().Trim();
                        mUbiGeo.Descripcion = row["Departamento"].ToString().Trim();

                        UbiGeoList.Add(mUbiGeo);
                    }
  
                }

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return UbiGeoList;
        }

        public List<UbiGeoModel> ListarProvincia(string departamento)
        {
            List<UbiGeoModel> UbiGeoList = new List<UbiGeoModel>();
            UbiGeoModel mUbiGeo = null;

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(new SqlParameter("@tt", 2));
                parametros.Add(new SqlParameter("@cod_dep", departamento));

                DataTable dtUbicacion = new Conexion().na_ExecuteDataTable("Usp_Ubicacion_Geografica", parametros);

                if ((dtUbicacion.Rows.Count > 0))
                {
                    foreach (DataRow row in dtUbicacion.Rows)
                    {
                        mUbiGeo = new UbiGeoModel();

                        mUbiGeo.Id = row["Codigo_Provincia"].ToString().Trim();
                        mUbiGeo.Descripcion = row["Provincia"].ToString().Trim();

                        UbiGeoList.Add(mUbiGeo);
                    }

                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return UbiGeoList;

        }

        public List<UbiGeoModel> ListarDistrito(string departamento, string provincia)
        {
            List<UbiGeoModel> UbiGeoList = new List<UbiGeoModel>();
            UbiGeoModel mUbiGeo = null;

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(new SqlParameter("@tt", 3));
                parametros.Add(new SqlParameter("@cod_dep", departamento));
                parametros.Add(new SqlParameter("@cod_prov", provincia));

                DataTable dtUbicacion = new Conexion().na_ExecuteDataTable("Usp_Ubicacion_Geografica", parametros);

                if ((dtUbicacion.Rows.Count > 0))
                {
                    foreach (DataRow row in dtUbicacion.Rows)
                    {
                        mUbiGeo = new UbiGeoModel();

                        mUbiGeo.Id = row["Codigo_Distrito"].ToString().Trim();
                        mUbiGeo.Descripcion = row["Distrito"].ToString().Trim();

                        UbiGeoList.Add(mUbiGeo);
                    }

                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return UbiGeoList;

        }

        public string GetCodigoUbicacion(string departamento, string provincia , string distrito)
        {
            var codigo = "";
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(new SqlParameter("@tt", 4));
                parametros.Add(new SqlParameter("@cod_dep", departamento));
                parametros.Add(new SqlParameter("@cod_prov", provincia));
                parametros.Add(new SqlParameter("@cod_distri", distrito));

                DataTable dtUbicacion = new Conexion().na_ExecuteDataTable("Usp_Ubicacion_Geografica", parametros);

                codigo = dtUbicacion.Rows[0]["Codigo_Ubicacion_Geografica"].ToString();

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return codigo;
        }
    }
}