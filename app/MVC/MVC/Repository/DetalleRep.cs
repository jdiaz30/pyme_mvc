﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MVC.Models;
using MVC.App_Start;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace MVC.Repository
{
    public class DetalleRep
    {
        public string Registrar(DetalleModel detalle)
        {
            string response = "0";

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@TT", 2));
                parametros.Add(new SqlParameter("@id_correlativo", detalle.Id));
                parametros.Add(new SqlParameter("@Tipo_Transaccion", detalle.Tipo_Tran));
                parametros.Add(new SqlParameter("@Codigo_Pro", detalle.Cod_Prod));
                parametros.Add(new SqlParameter("@Precio", detalle.Precio));
                parametros.Add(new SqlParameter("@cantidad", detalle.Cantidad));
                parametros.Add(new SqlParameter("@Total", detalle.Total));

                new Conexion().na_ExecuteNonQuery("Usp_Insertar_Detalle", parametros);

                ActualizarStock(detalle.Cantidad,detalle.Cod_Prod,detalle.Tipo_Tran);

                response = "1";

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return response;

        }

        public void ActualizarStock(string cantidad , string codProd,string tipoTran)
        {
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@tt", tipoTran));
                parametros.Add(new SqlParameter("@codigo_prod", codProd));
                parametros.Add(new SqlParameter("@cantidad", cantidad));

                new Conexion().na_ExecuteNonQuery("Usp_Actualizar_Stock_Producto", parametros);

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

        }
        public List<DetalleModel> Buscar(string idCorrelativo)
        {
            List<DetalleModel> detalleList = new List<DetalleModel>();
            DetalleModel mDetalle = null;

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@tt", 3));
                parametros.Add(new SqlParameter("@id_Correlativo",idCorrelativo));
                parametros.Add(new SqlParameter("@empresa", HttpContext.Current.Session["cod_empresa"]));

                DataTable dtDetalle = new Conexion().na_ExecuteDataTable("Usp_Consulta_Ventas", parametros);

                if (dtDetalle.Rows.Count > 0)
                {

                    foreach (DataRow row in dtDetalle.Rows)
                    {
                        mDetalle = new DetalleModel();

                        mDetalle.Cod_Prod = (row["Codigo_Producto"] != DBNull.Value ? row["Codigo_Producto"].ToString() : "");
                        mDetalle.Cantidad = (row["Cantidad"] != DBNull.Value ? row["Cantidad"].ToString() : "");
                        mDetalle.Descripcion = (row["Descripcion"] != DBNull.Value ? row["Descripcion"].ToString() : "");
                        mDetalle.Precio = (row["Precio"] != DBNull.Value ? row["Precio"].ToString() : "");
                        mDetalle.Unidad = (row["Unidad"] != DBNull.Value ? row["Unidad"].ToString() : "");
                        mDetalle.Marca = (row["Marca"] != DBNull.Value ? row["Marca"].ToString() : "");
                        mDetalle.Total = (row["Total"] != DBNull.Value ? row["Total"].ToString() : "");

                        detalleList.Add(mDetalle);
                    }
                }

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }
            return detalleList;
        }
    }
}