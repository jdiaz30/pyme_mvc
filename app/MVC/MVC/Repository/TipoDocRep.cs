﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MVC.Models;
using MVC.App_Start;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace MVC.Repository
{
    public class TipoDocRep
    {
        public List<TipoDocModel> Listar()
        {
            List<TipoDocModel> TipoDocList = new List<TipoDocModel>();
            TipoDocModel mTipoDoc = null;
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(new SqlParameter("@tt", 1));

                DataTable dtTipoDoc = new Conexion().na_ExecuteDataTable("Usp_Tipo_Documento", parametros);

                if ((dtTipoDoc.Rows.Count > 0))
                {
                    foreach (DataRow row in dtTipoDoc.Rows)
                    {
                        mTipoDoc = new TipoDocModel();

                        mTipoDoc.Codigo = (row["Codigo_Tipo_documento"] != DBNull.Value ? row["Codigo_Tipo_documento"].ToString() : ""); 
                        mTipoDoc.Descripcion = (row["Descripcion"] != DBNull.Value ? row["Descripcion"].ToString() : "");

                        TipoDocList.Add(mTipoDoc);
                    }

                }

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return TipoDocList;
        }
    }
}