﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MVC.Models;
using MVC.App_Start;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace MVC.Repository
{
    public class AccesoSistemaRep
    {
        public string Registrar(AccesoSistemaModel acceso)
        {
            string response = "0";

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@tipo", acceso.Tipo));
                parametros.Add(new SqlParameter("@usuario", acceso.Codigo_Usuario));
                parametros.Add(new SqlParameter("@fecha", Convert.ToDateTime(acceso.Fecha)));
                parametros.Add(new SqlParameter("@estado", acceso.Estado));

                new Conexion().na_ExecuteScalar("Usp_Insertar_Acceso", parametros);

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return response;
        }
        public string Actualizar(AccesoSistemaModel acceso)
        {
            string response = "0";

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@tipo", acceso.Tipo));
                parametros.Add(new SqlParameter("@usuario", acceso.Codigo_Usuario));
                parametros.Add(new SqlParameter("@fecha", acceso.Fecha));

                new Conexion().na_ExecuteScalar("Usp_Actualizar_Acceso", parametros);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return response;
        }
        public List<AccesoSistemaModel> validaAcceso(AccesoSistemaModel acceso)
        {
            List<AccesoSistemaModel> accesoList = new List<AccesoSistemaModel>();
            AccesoSistemaModel mAcceso = null;
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@tipo", acceso.Tipo));
                parametros.Add(new SqlParameter("@usuario", acceso.Codigo_Usuario));
                parametros.Add(new SqlParameter("@fecha", acceso.Fecha));
                parametros.Add(new SqlParameter("@estado", acceso.Estado));

                DataTable dtAcceso =new Conexion().na_ExecuteDataTable("Usp_Listar_Acceso_Usuario", parametros);

                foreach (DataRow row in dtAcceso.Rows)
                {
                    mAcceso = new AccesoSistemaModel();

                    mAcceso.Codigo_Usuario = (row["Codigo_Usuario"] != DBNull.Value ? row["Codigo_Usuario"].ToString() : "");
                    mAcceso.Fecha = (row["Fecha"] != DBNull.Value ? row["Fecha"].ToString() : "");
                    mAcceso.Estado = (row["Estado"] != DBNull.Value ? row["Estado"].ToString() : "");

                    accesoList.Add(mAcceso);
                }


            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }
            return accesoList;

        }
    }
}