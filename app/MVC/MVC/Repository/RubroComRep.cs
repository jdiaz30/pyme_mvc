﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MVC.Models;
using MVC.App_Start;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace MVC.Repository
{
    public class RubroComRep
    {
        public List<RubroComercialModel> Listar()
        {
            List<RubroComercialModel> RubroComList = new List<RubroComercialModel>();
            RubroComercialModel mRubroCom = null;

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(new SqlParameter("@tt", 3));

                DataTable dtGrupo = new Conexion().na_ExecuteDataTable("Usp_Grupo_Producto", parametros);

                if ((dtGrupo.Rows.Count > 0))
                {
                    foreach (DataRow row in dtGrupo.Rows)
                    {
                        mRubroCom = new RubroComercialModel();
                        
                        mRubroCom.Id = row["Codigo_Rubro_Comercial"].ToString().Trim();
                        mRubroCom.Descripcion = row["Descripcion_Rubro_Comercial"].ToString().Trim();

                        RubroComList.Add(mRubroCom);
                    }

                }

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return RubroComList;
        }
    }
}