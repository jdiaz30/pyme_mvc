﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MVC.Models;
using MVC.App_Start;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace MVC.Repository
{
    public class GrupoProdRep
    {
        public List<GrupoProdModel> Listar()
        {
            List<GrupoProdModel> GrupoProdList = new List<GrupoProdModel>();
            GrupoProdModel mGrupoProd = null;
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@tt", 1));
                parametros.Add(new SqlParameter("@length", 4));
                parametros.Add(new SqlParameter("@cod_rubro", HttpContext.Current.Session["rubro_com"]));

                DataTable dtGrupo = new Conexion().na_ExecuteDataTable("Usp_Grupo_Producto", parametros);

                if ((dtGrupo.Rows.Count > 0))
                {
                    foreach (DataRow row in dtGrupo.Rows)
                    {
                        mGrupoProd = new GrupoProdModel();

                        mGrupoProd.Codigo = (row["Codigo_Rubro_Comercial"] != DBNull.Value ? row["Codigo_Rubro_Comercial"].ToString() : "");
                        mGrupoProd.Descripcion = (row["Descripcion_Rubro_Comercial"] != DBNull.Value ? row["Descripcion_Rubro_Comercial"].ToString() : "");

                        GrupoProdList.Add(mGrupoProd);
                    }

                }

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return GrupoProdList;
        }
        public List<GrupoProdModel> ListarSubGrupo(string rubro)
        {
            List<GrupoProdModel> GrupoProdList = new List<GrupoProdModel>();
            GrupoProdModel mGrupoProd = null;
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@tt", 2));
                parametros.Add(new SqlParameter("@length", 6));
                parametros.Add(new SqlParameter("@cod_rubro", rubro));

                DataTable dtGrupo = new Conexion().na_ExecuteDataTable("Usp_Grupo_Producto", parametros);

                if ((dtGrupo.Rows.Count > 0))
                {
                    foreach (DataRow row in dtGrupo.Rows)
                    {
                        mGrupoProd = new GrupoProdModel();

                        mGrupoProd.Codigo = (row["Codigo_Rubro_Comercial"] != DBNull.Value ? row["Codigo_Rubro_Comercial"].ToString() : "");
                        mGrupoProd.Descripcion = (row["Descripcion_Rubro_Comercial"] != DBNull.Value ? row["Descripcion_Rubro_Comercial"].ToString() : "");

                        GrupoProdList.Add(mGrupoProd);
                    }

                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return GrupoProdList;

        }
    }
}