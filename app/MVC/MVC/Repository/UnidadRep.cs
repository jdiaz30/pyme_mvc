﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MVC.Models;
using MVC.App_Start;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace MVC.Repository
{
    public class UnidadRep
    {
        public List<UnidadModel> Listar()
        {
            List<UnidadModel> UnidadList = new List<UnidadModel>();
            UnidadModel mUnidad = null;
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@tt", 1));

                DataTable dtUnidad = new Conexion().na_ExecuteDataTable("Usp_Unidad_Medida", parametros);

                if ((dtUnidad.Rows.Count > 0))
                {
                    foreach (DataRow row in dtUnidad.Rows)
                    {
                        mUnidad = new UnidadModel();

                        mUnidad.Codigo = (row["Codigo_Unidad_Medida"] != DBNull.Value ? row["Codigo_Unidad_Medida"].ToString() : "");
                        mUnidad.Descripcion = (row["Descripcion"] != DBNull.Value ? row["Descripcion"].ToString() : "");

                        UnidadList.Add(mUnidad);
                    }

                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return UnidadList;
        }
    }
}