﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MVC.Models;
using MVC.App_Start;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace MVC.Repository
{
    public class CabeceraRep
    {
        public string Registrar(CabeceraModel cabecera)
        {
            string codigo = "01";

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@TT", 1));
                parametros.Add(new SqlParameter("@Fecha_Emision", cabecera.Fecha));
                parametros.Add(new SqlParameter("@Tipo_Transaccion", cabecera.Tipo_Tran));
                parametros.Add(new SqlParameter("@documento", cabecera.Documento));
                parametros.Add(new SqlParameter("@serie", cabecera.Serie));
                parametros.Add(new SqlParameter("@numero", cabecera.Numero));
                parametros.Add(new SqlParameter("@RUC", cabecera.Dni_Ruc));
                parametros.Add(new SqlParameter("@Cliente", cabecera.Cliente));
                parametros.Add(new SqlParameter("@Cantidad", cabecera.Cant_Prod));
                parametros.Add(new SqlParameter("@total", cabecera.Total));
                parametros.Add(new SqlParameter("@empresa", HttpContext.Current.Session["cod_empresa"]));
                parametros.Add(new SqlParameter("@usuario", HttpContext.Current.Session["id_usuario"]));
                parametros.Add(new SqlParameter("@rubro_comercial", HttpContext.Current.Session["rubro_com"]));

                codigo = new Conexion().na_ExecuteScalar("Usp_Insertar_Cabecera", parametros).ToString();

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return codigo;

        }
        public List<CabeceraModel> Buscar(string ruc, string fechaIni,string fechaFin,string tipoTran)
        {
            List<CabeceraModel> cabeceraList = new List<CabeceraModel>();
            CabeceraModel mCabecera = null;
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@tt", 2));
                parametros.Add(new SqlParameter("@Ruc", ruc.Trim()));
                parametros.Add(new SqlParameter("@Tipo_Transaccion", tipoTran));
                parametros.Add(new SqlParameter("@fecha1", fechaIni));
                parametros.Add(new SqlParameter("@fecha2", fechaFin));
                parametros.Add(new SqlParameter("@empresa", HttpContext.Current.Session["cod_empresa"]));

                DataTable dtCabecera = new Conexion().na_ExecuteDataTable("Usp_Consulta_Ventas", parametros);

                if (dtCabecera.Rows.Count > 0)
                {
                    foreach (DataRow row in dtCabecera.Rows)
                    {
                        mCabecera = new CabeceraModel();

                        mCabecera.Codigo = (row["Id"] != DBNull.Value ? row["Id"].ToString() : "");
                        mCabecera.Tipo_Tran = (row["Transaccion"] != DBNull.Value ? row["Transaccion"].ToString() : "");
                        mCabecera.Fecha = (row["fecha"] != DBNull.Value ? row["fecha"].ToString() : "");
                        mCabecera.Documento = (row["Documento"] != DBNull.Value ? row["Documento"].ToString() : "");
                        mCabecera.Dni_Ruc = (row["Ruc"] != DBNull.Value ? row["Ruc"].ToString() : "");
                        mCabecera.Cliente = (row["Cliente"] != DBNull.Value ? row["Cliente"].ToString() : "");
                        mCabecera.Total = (row["Total"] != DBNull.Value ? row["Total"].ToString() : "");

                        cabeceraList.Add(mCabecera);
                    }
                }

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return cabeceraList;
        }
        public List<CabeceraModel> Reporte(string idCabecera)
        {
            List<CabeceraModel> cabeceraList = new List<CabeceraModel>();
            CabeceraModel mCabecera = null;
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(new SqlParameter("@tt", 4));
                parametros.Add(new SqlParameter("@id_Correlativo", idCabecera.Trim()));
                parametros.Add(new SqlParameter("@empresa", HttpContext.Current.Session["cod_empresa"]));

                DataTable dtCabecera = new Conexion().na_ExecuteDataTable("Usp_Consulta_Ventas", parametros);

                if (dtCabecera.Rows.Count > 0)
                {
                    foreach (DataRow row in dtCabecera.Rows)
                    {
                        mCabecera = new CabeceraModel();

                        mCabecera.Fecha = (row["Fecha"] != DBNull.Value ? row["Fecha"].ToString() : "");
                        mCabecera.Documento = (row["Documento"] != DBNull.Value ? row["Documento"].ToString() : "");
                        mCabecera.Dni_Ruc = (row["Ruc"] != DBNull.Value ? row["Ruc"].ToString() : "");
                        mCabecera.Cliente = (row["Cliente"] != DBNull.Value ? row["Cliente"].ToString() : "");

                        cabeceraList.Add(mCabecera);
                    }
                }

            }catch(Exception ex){
                System.Diagnostics.Debug.WriteLine("Rep error" + ex);
            }

            return cabeceraList;
        }
    }
}